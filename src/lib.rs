extern crate proc_macro;
use proc_macro::TokenStream;

#[proc_macro]
pub fn string_hash(input: TokenStream) -> TokenStream {
    let string = input.into_iter().next().unwrap().to_string();
    let mut len = string.len();
    let mut bytes = string.bytes();
    let seed = 1337;
    let m = std::num::Wrapping(0x5bd1e995 as u32);
    let r = 24 as u32;

    let mut h = std::num::Wrapping(seed ^ string.len() as u32);

    while len >= 4 {
        let mut k = std::num::Wrapping(bytes.next().unwrap() as u32);
        k |= std::num::Wrapping((bytes.next().unwrap() as u32) << 8);
        k |= std::num::Wrapping((bytes.next().unwrap() as u32) << 16);
        k |= std::num::Wrapping((bytes.next().unwrap() as u32) << 24);

        k *= m;
        k ^= std::num::Wrapping(k.0 >> r);
        k *= m;

        h *= m;
        h ^= k;

        len -= 4;
    }

    if len == 3 {
        h ^= std::num::Wrapping((bytes.clone().nth(2).unwrap() as u32) << 16);
    }
    if len >= 2 {
        h ^= std::num::Wrapping((bytes.clone().nth(1).unwrap() as u32) << 8);
    }
    if len >= 1 {
        h ^= std::num::Wrapping(bytes.clone().nth(0).unwrap() as u32);
    }
    if len > 0 {
        h *= m;
    }

    h ^= h >> 13;
    h *= m;
    h ^= h >> 15;

    format!("({} as u64)", h.to_string()).parse().unwrap()
}
